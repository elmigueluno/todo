//
//  InputViewControllerTests.swift
//  ToDoTests
//
//  Created by Freddy Miguel Vega Zárate on 7/30/18.
//  Copyright © 2018 elmigueluno. All rights reserved.
//

import XCTest
import CoreLocation
@testable import ToDo

extension InputViewControllerTests {
    
    class MockGeocoder: CLGeocoder {
        
        var completionHandler: CLGeocodeCompletionHandler?
        
        override func geocodeAddressString(_ addressString: String, completionHandler: @escaping CLGeocodeCompletionHandler) {
            self.completionHandler = completionHandler
        }
    }
    
    class MockPlacemark : CLPlacemark {
        
        var mockCoordinate: CLLocationCoordinate2D?
        
        override var location: CLLocation? {
            guard let coordinate = mockCoordinate else {
                return CLLocation()
            }
            
            return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
    }
}

class InputViewControllerTests: XCTestCase {
    
    var sut: InputViewController!
    var placemark: MockPlacemark!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: String(describing: InputViewController.self)) as! InputViewController
        sut.loadViewIfNeeded()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func isSubview(of parent: UIView, subview: UIView) -> Bool {
        return subview.isDescendant(of: parent)
    }
    
    func test_HasTitleTextField() {
        XCTAssertTrue(self.isSubview(of: sut.view, subview: sut.titleTextField))
    }
    
    func test_HasDateTextField() {
        XCTAssertTrue(self.isSubview(of: sut.view, subview: sut.dateTextField))
    }
    
    func test_HasLocationTextField() {
        XCTAssertTrue(self.isSubview(of: sut.view, subview: sut.locationTextField))
    }
    
    func test_HasAddressTextField() {
        XCTAssertTrue(self.isSubview(of: sut.view, subview: sut.addressTextField))
    }
    
    func test_HasDescriptionTextField() {
        XCTAssertTrue(self.isSubview(of: sut.view, subview: sut.descriptionTextField))
    }
    
    func test_HasSaveButton() {
        XCTAssertTrue(self.isSubview(of: sut.view, subview: sut.saveButton))
    }
    
    func test_Save_UsesGeocoderToGetCoordinateFromAddress() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ssZZZZZ"
        
        let timestamp = 1456095600.0
        let date = Date(timeIntervalSince1970: timestamp)
        
        sut.titleTextField.text = "Foo"
        sut.dateTextField.text = dateFormatter.string(from: date)
        sut.locationTextField.text = "Bar"
        sut.addressTextField.text = "Infinite Loop 1, Cupertino"
        sut.descriptionTextField.text = "Baz"
        
        let mockGeocoder = MockGeocoder()
        sut.geocoder = mockGeocoder
        
        sut.itemManager = ItemManager()
        
        sut.save()
        
        placemark = MockPlacemark()
        let coordinate = CLLocationCoordinate2DMake(37.3316851, -122.0300674)
        
        placemark.mockCoordinate = coordinate
        mockGeocoder.completionHandler?([placemark], nil)
        
        let item = sut.itemManager?.item(at: 0)
        
        let testItem = ToDoItem(title: "Foo", itemDescription: "Baz", timestamp: timestamp, location: Location(name: "Bar", coordinate: coordinate))
        
        XCTAssertEqual(item, testItem)
    }
    
    
    func test_Save_WithoutTitle() {
        sut.itemManager = ItemManager()
        sut.save()
        
        XCTAssertNil(sut.itemManager?.item(at: 0))
    }
    
    
    func test_Save_WithoutDate() {
        sut.titleTextField.text = "Foo"
        sut.locationTextField.text = "Bar"
        sut.addressTextField.text = "Infinite Loop 1, Cupertino"
        sut.descriptionTextField.text = "Baz"
        
        let mockGeocoder = MockGeocoder()
        sut.geocoder = mockGeocoder
        
        sut.itemManager = ItemManager()
        
        sut.save()
        
        placemark = MockPlacemark()
        placemark.mockCoordinate = CLLocationCoordinate2DMake(37.3316851, -122.0300674)
        mockGeocoder.completionHandler?([placemark], nil)
        
        let item = sut.itemManager?.item(at: 0)
        
        XCTAssertNotNil(item)
        XCTAssertNil(item?.timestamp)
    }
    
    func test_SaveButtonHasSaveAction() {
        let saveButton: UIButton = sut.saveButton
        guard let actions = saveButton.actions(forTarget: sut, forControlEvent: .touchUpInside) else {
            XCTFail()
            return
        }
        
        XCTAssertTrue(actions.contains("save"))
    }
    
    func test_Geocoder_FetchesCoordinates() {
        let address = "Infinite Loop 1, Cupertino"
        let geocoderAnswered = expectation(description: "Geocoder")
        
        CLGeocoder().geocodeAddressString(address) {
            (placemarks, error) -> Void in
            let coordinate = placemarks?.first?.location?.coordinate
            
            guard let latitude = coordinate?.latitude else {
                XCTFail()
                return
            }
            
            guard let longitude = coordinate?.longitude else {
                XCTFail()
                return
            }
            
            XCTAssertEqual(latitude, 37.3316, accuracy: 0.001)
            XCTAssertEqual(longitude, -122.0300, accuracy: 0.001)
            
            geocoderAnswered.fulfill()
        }
        
        waitForExpectations(timeout: 3, handler: nil)
    }
}
