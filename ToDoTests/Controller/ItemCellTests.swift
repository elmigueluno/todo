//
//  ItemCellTests.swift
//  ToDoTests
//
//  Created by Freddy Miguel Vega Zarate on 26-07-18.
//  Copyright © 2018 elmigueluno. All rights reserved.
//

import XCTest
import CoreLocation
@testable import ToDo

extension ItemCellTests {
    
    class FakeDataSource: NSObject, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return UITableViewCell()
        }
    }
}

class ItemCellTests: XCTestCase {
    
    var tableView: UITableView!
    var cell: ItemCell!
    var dateFormatter: DateFormatter!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ItemListViewController") as! ItemListViewController
        controller.loadViewIfNeeded()
        
        tableView = controller.tableView
        tableView?.dataSource = FakeDataSource()
        
        cell = tableView?.dequeueReusableCell(withIdentifier: "ItemCell", for: IndexPath(row: 0, section: 0)) as! ItemCell
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_HasNameLabel() {
        XCTAssertTrue(cell.titleLabel.isDescendant(of: cell.contentView))
    }
    
    func test_HasLocationLabel() {
        XCTAssertTrue(cell.locationLabel.isDescendant(of: cell.contentView))
    }
    
    func test_ConfigCell_SetsTitle() {
        cell.configCell(with: ToDoItem(title: "Foo"))
        XCTAssertEqual(cell.titleLabel.text, "Foo")
    }
    
    func test_ConfigCell_SetsDate() {
        let date = dateFormatter.date(from: "08/27/2017")
        let timestamp = date?.timeIntervalSince1970
        cell.configCell(with: ToDoItem(title: "Foo", timestamp: timestamp))
        
        XCTAssertEqual(cell.dateLabel.text, "08/27/2017")
    }
    
    
    func test_ConfigCell_SetsLocation() {
        let location = Location(name: "Bar")
        cell.configCell(with: ToDoItem(title: "Foo", location: location))
        
        XCTAssertEqual(cell.locationLabel.text, location.name)
    }
    
    func test_LocationLabelText_EmptyWhenItemDoesNotHasLocation() {
        cell.configCell(with: ToDoItem(title: "Foo"))
        
        XCTAssertNil(cell.locationLabel.text)
    }
    
    func test_DateLabelText_EmptyWhenItemDoesNotHasDate() {
        cell.configCell(with: ToDoItem(title: "Foo"))
        
        XCTAssertNil(cell.dateLabel.text)
    }
    
    func test_Title_WhenItemIsChecked_IsStrokeThrough() {
        let location = Location(name: "Bar")
        let item = ToDoItem(title: "Foo", itemDescription: nil, timestamp: 1456150025, location: location)
        cell.configCell(with: item, checked: true)
        
        let attributtedString = NSAttributedString(string: "Foo", attributes: [NSAttributedStringKey.strikethroughStyle: NSUnderlineStyle.styleSingle.rawValue]
        )
        
        XCTAssertEqual(cell.titleLabel.attributedText, attributtedString)
        XCTAssertNil(cell.locationLabel.text)
        XCTAssertNil(cell.dateLabel.text)
    }
}
