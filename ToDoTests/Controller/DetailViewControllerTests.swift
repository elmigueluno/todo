//
//  DetailViewControllerTests.swift
//  ToDoTests
//
//  Created by Freddy Miguel Vega Zarate on 30-07-18.
//  Copyright © 2018 elmigueluno. All rights reserved.
//

import XCTest
import CoreLocation
@testable import ToDo

class DetailViewControllerTests: XCTestCase {
    
    var sut: DetailViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: String(describing: DetailViewController.self)) as! DetailViewController
        sut.loadViewIfNeeded()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_HasTitleLabel() {
        let labelIsSubview = sut.titleLabel.isDescendant(of: sut.view)
        
        XCTAssertTrue(labelIsSubview)
    }
    
    func test_HasDateLabel() {
        let labelIsSubview = sut.dateLabel.isDescendant(of: sut.view)
        
        XCTAssertTrue(labelIsSubview)
    }
    
    func test_HasLocationLabel() {
        let labelIsSubview = sut.locationLabel.isDescendant(of: sut.view)
        
        XCTAssertTrue(labelIsSubview)
    }
    
    func test_HasDescriptionLabel() {
        let labelIsSubview = sut.descriptionLabel.isDescendant(of: sut.view)
        
        XCTAssertTrue(labelIsSubview)
    }
    
    func test_HasMapView() {
        let mapViewIsSubView = sut.mapView?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(mapViewIsSubView)
    }
    
    func test_SettingItemInfo_SetsTextsToLabels() {
        let item = makeToDoItem(title: "Bar", description: "Baz", timestamp: 1456150025, locationName: "Foo", latLong: nil)
        let itemManager = ItemManager()
        itemManager.add(item)
        sut.itemInfo = (itemManager, 0)
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        
        XCTAssertEqual(sut.titleLabel.text, "Bar")
        XCTAssertEqual(sut.dateLabel.text, "02/22/2016")
        XCTAssertEqual(sut.locationLabel.text, "Foo")
        XCTAssertEqual(sut.descriptionLabel.text, "Baz")
    }
    
    func test_SettingItemInfo_SetsLocationToMap() {
        let item = makeToDoItem(title: "Bar", description: "Baz", timestamp: 1456150025, locationName: "Foo", latLong: (51.2277, 6.7735))
        let itemManager = ItemManager()
        itemManager.add(item)
        sut.itemInfo = (itemManager, 0)
        sut.beginAppearanceTransition(true, animated: true)
        sut.endAppearanceTransition()
        
        if let latitude = item.location?.coordinate?.latitude, let longitude = item.location?.coordinate?.longitude {
            XCTAssertEqual(sut.mapView.centerCoordinate.latitude, latitude, accuracy: 0.001)
            XCTAssertEqual(sut.mapView.centerCoordinate.longitude, longitude, accuracy: 0.001)
        }
        else {
            XCTFail()
        }
    }
    
    func test_CheckItem_ChecksItemInItemManager() {
        let itemManager = ItemManager()
        itemManager.add(ToDoItem(title: "Foo"))
        sut.itemInfo = (itemManager, 0)
        sut.checkItem()
        
        XCTAssertEqual(itemManager.toDoCount, 0)
        XCTAssertEqual(itemManager.doneCount, 1)
    }
    
    func makeToDoItem(title: String, description: String?, timestamp: Double?, locationName: String?, latLong: (Double, Double)?) -> ToDoItem {
        
        var coordinate: CLLocationCoordinate2D? = nil
        if let latLong = latLong {
            coordinate = CLLocationCoordinate2D(latitude: latLong.0, longitude: latLong.1)
        }
        
        var location: Location? = nil
        if let locationName = locationName, let coordinate = coordinate {
            location = Location(name: locationName, coordinate: coordinate)
        }
        else if let coordinate = coordinate {
            location = Location(name: "", coordinate: coordinate)
        }
        else if let locationName = locationName {
            location = Location(name: locationName)
        }
        
        return ToDoItem(title: title, itemDescription: description, timestamp: timestamp, location: location)
    }
}
