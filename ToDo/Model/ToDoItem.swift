//
//  ToDoItem.swift
//  ToDo
//
//  Created by Freddy Miguel Vega Zarate on 23-07-18.
//  Copyright © 2018 elmigueluno. All rights reserved.
//

import Foundation

struct ToDoItem: Equatable {
    
    let title: String
    let itemDescription: String?
    let timestamp: Double?
    let location: Location?
    
    static func == (lhs: ToDoItem, rhs: ToDoItem) -> Bool {
        return lhs.title == rhs.title
                && lhs.itemDescription == rhs.itemDescription
                && lhs.timestamp == rhs.timestamp
                && lhs.location == rhs.location
    }
    
    init(title: String, itemDescription: String? = nil, timestamp: Double? = nil, location:Location? = nil) {
        self.title = title
        self.itemDescription = itemDescription
        self.timestamp = timestamp
        self.location = location
    }
}
