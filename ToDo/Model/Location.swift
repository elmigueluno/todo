//
//  Location.swift
//  ToDo
//
//  Created by Freddy Miguel Vega Zarate on 23-07-18.
//  Copyright © 2018 elmigueluno. All rights reserved.
//

import Foundation
import CoreLocation

struct Location: Equatable {
    
    let name: String
    let coordinate: CLLocationCoordinate2D?
    
    static func == (lhs: Location, rhs: Location) -> Bool {
        return  lhs.name == rhs.name
            && lhs.coordinate?.latitude == rhs.coordinate?.latitude
            && lhs.coordinate?.longitude == rhs.coordinate?.longitude
    }
    
    init(name: String, coordinate: CLLocationCoordinate2D? = nil) {
        self.name = name
        self.coordinate = coordinate
    }
}
