//
//  ItemManager.swift
//  ToDo
//
//  Created by Freddy Miguel Vega Zarate on 23-07-18.
//  Copyright © 2018 elmigueluno. All rights reserved.
//

import Foundation

class ItemManager {
    var toDoCount: Int {
        return toDoItems.count
    }
    var doneCount: Int {
        return doneItems.count
    }
    private var toDoItems: [ToDoItem] = []
    private var doneItems: [ToDoItem] = []
    
    func add(_ item: ToDoItem) {
        if !toDoItems.contains(item) {
            toDoItems.append(item)
        }
    }
    
    func item(at index: Int) -> ToDoItem? {
        if (index < toDoItems.count) {
            return toDoItems[index]
        }
        return nil
    }
    
    func doneItem(at index: Int) -> ToDoItem? {
        if (index < doneItems.count) {
            return doneItems[index]
        }
        return nil
    }
    
    func checkItem(at index: Int) {
        if (index < toDoItems.count) {
            let chekedItem = toDoItems.remove(at: index)
            doneItems.append(chekedItem)
        }
    }
    
    func uncheckItem(at index: Int) {
        if (index < doneItems.count) {
            let unchekedItem = doneItems.remove(at: index)
            toDoItems.append(unchekedItem)
        }
    }
    
    func removeAll() {
        toDoItems.removeAll()
        doneItems.removeAll()
    }
}
