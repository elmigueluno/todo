//
//  ItemCell.swift
//  ToDo
//
//  Created by Freddy Miguel Vega Zarate on 26-07-18.
//  Copyright © 2018 elmigueluno. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter
    }()
    
    func configCell(with item: ToDoItem, checked: Bool = false) {
        titleLabel.text = item.title
        
        if checked {
            let attributtedString = NSAttributedString(string: item.title, attributes: [NSAttributedStringKey.strikethroughStyle: NSUnderlineStyle.styleSingle.rawValue]
            )
            titleLabel.attributedText = attributtedString
            locationLabel.text = nil
            dateLabel.text = nil
        }
        else {
            locationLabel.text = nil
            if let locationName = item.location?.name {
                locationLabel.text = locationName
            }
            
            dateLabel.text = nil
            if let timestamp = item.timestamp {
                let date = Date(timeIntervalSince1970: timestamp)
                dateLabel.text = dateFormatter.string(from: date)
            }
        }
    }
}
